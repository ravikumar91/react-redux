import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import { simpleAction } from './actions/simpleAction';
import Login from './containers/Login';


class App extends Component {
  
  render() {
   return (
  
    <div className="App">
      <Login />
    </div>
   );
  }
 }
 const mapStateToProps = state => ({
  ...state
 })
 const mapDispatchToProps = dispatch => ({
  simpleAction: () => dispatch(simpleAction())
 })
 export default connect(mapStateToProps, mapDispatchToProps)(App);

