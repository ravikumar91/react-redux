import React from 'react'
import { connect } from 'react-redux'
import { login } from '../actions/login'

 const Login = ({ dispatch }) => {
  let email;
  let password;

  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        dispatch(login(email.value, password.value))
        email.value = ''
        password.value = ''
      }}>
        <input ref={node => email = node} />
        <input ref={node => password = node} />
        <button type="submit">
         Login
        </button>
      </form>
    </div>
  )
}

export default connect()(Login)