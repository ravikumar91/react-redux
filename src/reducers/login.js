const login = (state = [], action) => {
    switch (action.type) {
      case 'LOGIN':
        return [
          ...state,
          {
            password: action.password,
            email: action.email,
            completed: false
          }
        ]
      default:
        return state
    }
  }
  
  export default login