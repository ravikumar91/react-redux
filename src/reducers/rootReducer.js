import { combineReducers } from 'redux';
import simpleReducer from './simpleReducer';
import login from './login';
export default combineReducers({
    login,
 simpleReducer
});